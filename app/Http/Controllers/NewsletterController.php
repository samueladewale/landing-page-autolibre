<?php

namespace App\Http\Controllers;

use App\Model\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function store(Request $request) {
    	$mail = $request->input('mail');
    	$newsletter = new Newsletter;

    	if ( !$mail ) {
    		throw new Exception("No mail provided", 1);
    	};

    	if ($this->hasMail($mail)) {
    		return redirect()->back()->with('status', 'conflict');
    	}

    	$newsletter->mail = $mail;
    	$newsletter->save();

    	return redirect()->back()->with('status', 'success');
    }

    protected function hasMail($mail) {
    	$mail = Newsletter::where('mail', $mail)->first();

    	if (!$mail) { return false; }

    	return true;
    } 
}
