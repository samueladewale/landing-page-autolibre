<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Autolibre</title>
	<link rel="stylesheet" href="">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-with, initial-scale=1.0">
	<meta name="description" content="Autolibre">
	<meta name="keywords" content="autolibre, covoiturage">
	<link rel="icon" href="/favicon.ico">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>
<body>
	<div class="page">
		<div class="site-content">
			<main id="main">
				<!-- header -->
				<header class="py-4">
					<div class="wrapper d-flex flex-row justify-content-between align-items-center">
						<div class="site-logo">
							<a href="/home">
								<img src="/img/logo-autolibre.png" alt="Logo autolibre" width="200px">
							</a>
						</div>

						<div class="features d-lg-block d-none">
							<ul class="feature-list d-flex align-items-center m-0">
								<li class="ml-4">LOCATION SIMPLE</li>
								<li class="ml-4">COVOITURAGE</li>
								<li class="ml-4">AUTO-PARTAGE</li>
							</ul>
						</div>
					</div>
				</header>
				<!-- /header -->

				<section class="message d-flex align-items-center justify-content-center">
					<div class="content">
						<div class="text-content d-flex align-items-center flex-lg-row flex-column">
							<div class="image">
								<img src="/img/panneau.png" alt="Site en contruction" width="200px">
							</div>
							<div class="text pl-3">
								<h1 class="h1">OOOOUPS &nbsp;!</h1>
								<h3>SITE EN CONSTRUCTION</h3>
								<div class="loader-container mt-3 d-lg-block d-none">
									<div class="bar-container w-100 bg-white">
										<div class="bar h-100" style="width: 80%"></div>
									</div>
									<span id="percentage">80%</span>
								</div>
							</div>
						</div>
						<form action="/newsletter" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="row">
								<div class="col-lg-7 col-sm-12">
									<input type="email" name="mail" value="" placeholder="Email" class="w-100">
								</div>
								<div class="col-lg-5 pl-lg-0 mt-lg-0 mt-3 col-sm-12">
									<button type="submit" class="btn bg-white w-100">ME TENIR INFORMÉ(E)</button>
								</div>
							</div>
							@if (session('status') === 'success')
								<div class="alert alert-success mt-2">
									<small>Votre mail a bien été enregistré. Merci de rester à l'écoute<small>
								</div>
							@endif
							@if (session('status') === 'conflict')
								<div class="alert alert-info mt-2">
									<small>Votre mail est déja enregistré. Merci de rester à l'écoute<small>
								</div>
							@endif
						</form>
					</div>
				</section>

			</main>

			<!-- Footer -->
			<footer>
				<div class="content col-lg-12 bg-dark d-flex justify-content-between align-items-center py-2">
					<div class="footer-logo">
						<img src="/img/logo-tts.png" alt="tts-ci" width="80px">
					</div>
					<div class="copyright text-white-50 d-lg-block d-none" style="font-size: 10px">
						© <?php echo date('Y'); ?> AUTOLIBRE TOUS DROITS RÉSERVÉS
					</div>
					<div class="socials">
						<ul class="social-list d-flex m-0">
							<li class="ml-3">
								<a href="https://facebook.com/djobacash" title="" class="text-white-50">
									<i class="fab fa-facebook-square"></i>	
								</a>
							</li>
							<li class="ml-3">
								<a href="https://instagram.com/djoba_cash" title="" class="text-white-50">
									<i class="fab fa-instagram"></i>	
								</a>
							</li>
							<li class="ml-3">
								<a href="https://twitter.com/Djobacash" title="" class="text-white-50">
									<i class="fab fa-twitter"></i>	
								</a>
							</li>
							<li class="ml-3">
								<a href="https://youtube.com/channel/UC8Iedms7BwGBQ8DQBShu5xg" title="" class="text-white-50">
									<i class="fab fa-youtube"></i>	
								</a>
							</li>
						</ul>
					</div>
				</div>	
			</footer>
			<!-- /Footer -->
		</div>
	</div>
</body>
</html>
<html